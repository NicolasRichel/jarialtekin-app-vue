/** @type {Service} */
const service = {
  async fetchProjects() {
    // TODO: fetch projects from backend service
    return await fetch("dev/dataset.json").then(res => res.json()).then(data => data.projects);
  },

  async fetchTasks() {
    // TODO: fetch tasks from backend service
    return await fetch("dev/dataset.json").then(res => res.json()).then(data => data.tasks);
  },

  async fetchStatuses() {
    // TODO: fetch statuses from backend service
    return await fetch("dev/dataset.json").then(res => res.json()).then(data => data.statuses);
  },

  async fetchPriorities() {
    // TODO: fetch priorities from backend service
    return await fetch("dev/dataset.json").then(res => res.json()).then(data => data.priorities);
  },
};

export default service;
