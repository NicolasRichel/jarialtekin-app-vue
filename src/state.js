import { reactive, readonly, toRefs } from "vue";
import service from "./service.js";

/** @type {State} */
const state = reactive({
  projects: [],
  tasks: [],
  statuses: [],
  priorities: [],
});

async function loadData() {
  state.projects = await service.fetchProjects();
  state.tasks = await service.fetchTasks();
  state.statuses = await service.fetchStatuses();
  state.priorities = await service.fetchPriorities();
}

export function useState() {
  return {
    ...toRefs(readonly(state)),
    loadData,
  };
}
