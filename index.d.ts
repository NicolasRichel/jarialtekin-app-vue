interface Project {
  id: string;
  name: string;
  description: string;
}

interface Task {
  id: string;
  name: string;
  description: string;
  status: number;
  priority: number;
}

interface Status {
  id: string;
  index: number;
  name: string;
}

interface Priority {
  id: string;
  index: number;
  name: string;
}

// ---

interface Service {
  fetchProjects: () => Promise<Project[]>;
  fetchTasks: () => Promise<Task[]>;
  fetchStatuses: () => Promise<Status[]>;
  fetchPriorities: () => Promise<Priority[]>;
}

interface State {
  projects: Project[];
  tasks: Task[];
  statuses: Status[];
  priorities: Priority[];
}
